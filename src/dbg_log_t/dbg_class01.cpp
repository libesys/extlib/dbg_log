/*!
 * \file dbg_log_t/dbg_class01.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2015 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "dbg_log_t/dbg_log_t_prec.h"
#include <dbg_log/inttypes.h>
#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>
#include <fstream>
#include <iostream>

#include <boost/tokenizer.hpp>

class Test01
{
public:
    Test01();
    ~Test01();

    void DoBasic01(uint32_t p0, uint16_t p1, uint8_t p2);
    void DoBasic02(uint32_t p0, uint16_t p1, uint8_t p2);
    void DoBasic03(uint32_t p0, uint16_t p1, uint8_t p2);
    void DoBasic04(uint32_t p0, uint32_t p1, uint8_t p2);
    void DoBasic05(uint32_t p0, uint32_t p1, uint8_t p2);
    void DoBasic06(uint32_t p0, uint32_t p1, uint8_t p2);
    void DoBasic07(uint32_t p0, uint32_t &p1, uint8_t p2);
    int DoBasic08(uint32_t p0, uint32_t &p1, uint8_t p2);
};

Test01::Test01()
{
}

Test01::~Test01()
{
}

void Test01::DoBasic01(uint32_t p0, uint16_t p1, uint8_t p2)
{
    DBG_CALLMEMBER("Test01::DoBasic01", false);
    DBG_PARAM(0, "p0", uint32_t, p0);
    DBG_PARAM(1, "p1", uint16_t, p1);
    DBG_PARAM(2, "p2", uint8_t, p2);
    DBG_CALLMEMBER_END;
}

#define DBG_TPARAM(name,type,var) \
    dbg_log::dbg_class_param<type> __up_dbg_class_param##name(#name,var); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##name);

void Test01::DoBasic02(uint32_t p0, uint16_t p1, uint8_t p2)
{
    DBG_CALLMEMBER("Test01::DoBasic02", false);
    DBG_TPARAM(p0, uint32_t, p0);
    DBG_TPARAM(p1, uint16_t, p1);
    DBG_TPARAM(p2, uint8_t, p2);
    DBG_CALLMEMBER_END;
}

void Test01::DoBasic03(uint32_t p0, uint16_t p1, uint8_t p2)
{
    dbg_log::dbg_class_param<uint32_t> __up_dbg_class_param_ext("p0", p0);

    DBG_CALLMEMBER("Test01::DoBasic03", false);
    DBG_P(p0);
    DBG_P(p1);
    DBG_P(p2);
    DBG_CALL_END;
}

void Test01::DoBasic04(uint32_t p0, uint32_t p1, uint8_t p2)
{
    DBG_CALL_BEGIN(false);
    DBG_P(p0);
    DBG_P(p1);
    DBG_P(p2);
    DBG_CALL_END;
}

void Test01::DoBasic05(uint32_t p0, uint32_t p1, uint8_t p2)
{
    DBG_CALL_BEGIN(false);
    DBG_P(p0);
    DBG_P(p1);
    DBG_P(p2);
    DBG_CALL_END;
}

#include <tuple>

#define DBG_GCALLMEMBER(print_state, ...) \
	static dbg_log::dbg_class_call_helper __up_dbg_class_member_call_test(__FUNCSIG__, #__VA_ARGS__); \
	auto tt2 = make_mytuple(__VA_ARGS__); \
	tt2.SetNames(__up_dbg_class_member_call_test.m_names); \
    dbg_log::dbg_class __up_dbg_class_member_call(__FUNCSIG__, print_state); \
    __up_dbg_class_member_call.SetFunctionInfo(&__up_dbg_class_member_call_test.m_info); \
    __up_dbg_class_member_call_test.m_prof.IncCall(); \
    __up_dbg_class_member_call_test.m_prof.Start(); \
    dbg_log::dbg_class_prof_bug __up_dbg_class_member_call_prof_bug(&__up_dbg_class_member_call_test.m_prof) \


template<typename... Types>
std::tuple<const char *, Types...> create_dbg_class_param_tuple(const char *name, Types... types)
{
    std::tuple<const char *, Types...> s_tuple(name, types...);

    return s_tuple;
}

template <class... Ts> struct mytuple
{
    void SetNames(const std::vector<std::string> &names)
    {
        assert(names.size() == m_vec.size());

        for (unsigned int idx = 0; idx < names.size(); ++idx)
            m_vec[idx]->SetName(names[names.size() - idx - 1].c_str());
    }

    void Print(std::ostream &os)
    {
        std::vector<dbg_log::dbg_class_param_base *>::reverse_iterator it;

        for (it = m_vec.rbegin(); it != m_vec.rend(); ++it)
        {
            (*it)->Print(os);
            os << std::endl;
        }
    }

    dbg_log::dbg_class_param_base *operator[](unsigned int idx)
    {
        if (m_vec.size() == 0)
            return nullptr;
        if (idx >= m_vec.size())
            return nullptr;
        return m_vec[m_vec.size() - idx - 1];
    }

    std::vector<dbg_log::dbg_class_param_base *> m_vec;
};

template<class T>
struct mytuple_helper
{
    mytuple_helper(const char *name, T &t) : m_name(name), m_t(t)
    {
    }

    const char *m_name;
    T &m_t;
};

template<typename T>
mytuple_helper<T> make_mytuple_helper(const char *name, T &t)
{
    return mytuple_helper<T>(name, t);
}

template <class T, class... Ts>
struct mytuple<T, Ts...> : mytuple<Ts...>
{
    mytuple(T &t, Ts&... ts) : mytuple<Ts...>(ts...), /*m_t(&t),*/ m_tail(t)
    {
        this->m_vec.push_back(&m_tail);
    }

    mytuple(mytuple_helper<T> &t, mytuple_helper<Ts>&... ts) : mytuple<Ts...>(ts...), /*m_t(&t),*/ m_tail(t.m_name, t.m_t)
    {
        this->m_vec.push_back(&m_tail);
    }

    //T *m_t;
    dbg_log::dbg_class_param<T> m_tail;
};

template <size_t, class> struct elem_type_holder;

template <class T, class... Ts>
struct elem_type_holder<0, mytuple<T, Ts...>>
{
    typedef dbg_log::dbg_class_param<T> type;
};

template <size_t k, class T, class... Ts>
struct elem_type_holder<k, mytuple<T, Ts...>>
{
    typedef typename elem_type_holder<k - 1, mytuple<Ts...>>::type type;
};

template <size_t k, class... Ts>
typename std::enable_if<k == 0, typename elem_type_holder<0, mytuple<Ts...>>::type&>::type
        get(mytuple<Ts...>& t)
{
    return t.m_tail;
}

template <size_t k, class T, class... Ts>
typename std::enable_if<k != 0, typename elem_type_holder<k, mytuple<T, Ts...>>::type&>::type
        get(mytuple<T, Ts...>& t)
{
    mytuple<Ts...>& base = t;
    return get<k - 1>(base);
}

template<class T, class... Ts>
mytuple<T, Ts...> make_mytuple(mytuple_helper<T> &&t, mytuple_helper<Ts>&&... ts)
{
    return mytuple<T, Ts...>(t, ts...);
}

template<class T, class... Ts>
mytuple<T, Ts...> make_mytuple(T &t, Ts&... ts)
{
    return mytuple<T, Ts...>(t, ts...);
}

/*template<class T, class... Ts, template<typename> class Arg, template<typename> class... Args>
mytuple<T, Ts...> make_mytuple2(Arg<T> &arg, Args<Ts>&... args)
{
    auto
    return mytuple<T, Ts...>(mytuple_helper<T>(arg), mytuple_helper(args...));
} */

void Test01::DoBasic06(uint32_t p0, uint32_t p1, uint8_t p2)
{
    DBG_GCALLMEMBER(false, p0, p1, p2);
    DBG_P(p0);
    DBG_P(p1);
    DBG_P(p2);
    DBG_CALL_END;
}

void Test01::DoBasic07(uint32_t p0, uint32_t &p1, uint8_t p2)
{
    DBG_CALL_BEGIN(false);
    DBG_P(p0);
    DBG_P_OUT(p1);
    DBG_P(p2);
    DBG_CALL_END;

    p1 = 45;
}

int Test01::DoBasic08(uint32_t p0, uint32_t &p1, uint8_t p2)
{
    DBG_CALL_RET_DEF_BEGIN(false, int, result);
    DBG_P(p0);
    DBG_P_OUT(p1);
    DBG_P(p2);
    DBG_CALL_END;

    p1 = 45;
    result = -7;
    return result;
}



BOOST_AUTO_TEST_CASE(DbgClass01)
{
    std::ofstream ofs("dbg_class01.txt");
    logmod::logger logger;
    Test01 test01;

    logger.SetOfstream(&ofs);
    dbg_log::dbg_class::SetDefaultLogger(&logger);
    dbg_log::dbg_class::Enable();

    test01.DoBasic01(13, 21, 45);
    test01.DoBasic02(13, 21, 45);
    test01.DoBasic03(13, 21, 45);
    test01.DoBasic04(13, 21, 45);
    test01.DoBasic05(13, 21, 45);
    test01.DoBasic06(13, 21, 45);

    uint32_t value32 = 21;

    test01.DoBasic07(13, value32, 45);
    test01.DoBasic08(13, value32, 45);

    auto t = create_dbg_class_param_tuple("lalal", 1, (uint32_t)3);
    std::cout << std::get<0>(t) << std::endl;

    uint32_t p0 = 10;
    int p1 = 11;
    uint32_t p2 = 12;

    mytuple_helper<uint32_t> h("p0", p0);

    mytuple<uint32_t, int, uint32_t> tt(p0, p1, p2);
    mytuple<uint32_t, int, uint32_t> tt1(mytuple_helper<uint32_t>("p0", p0), mytuple_helper<int>("p1", p1), mytuple_helper<uint32_t>("p2", p2));

    auto tt2 = make_mytuple(p0, p1, p2);
    auto tt3 = make_mytuple(make_mytuple_helper("p0", p0), make_mytuple_helper("p1", p1));
    //auto tt4 = make_mytuple2({ "p0", p0 }, { "p1", p1 });
    //std::cout << "vec size " << tt.m_vec.size() << std::endl;

    p0 = 19;
    p1 = 23;

    std::cout << "p0 = " << p0 << std::endl;
    std::cout << "p1 = " << p1 << std::endl;
    std::cout << "p2 = " << p2 << std::endl;

    tt.Print(std::cout);
    tt1.Print(std::cout);

    tt[0]->SetName("p0");
    tt[0]->Print(std::cout);

    /*for (std::size_t idx = 0; idx < tt.m_vec.size(); ++idx)
    {
        std::cout << "[" << idx << "] = ";
        tt.m_vec[idx]->Print(std::cout);
        std::cout << std::endl;
    }*/
    get<0>(tt).Print(std::cout);
    std::cout << std::endl;
    get<1>(tt).Print(std::cout);
    std::cout << std::endl;
    get<2>(tt).Print(std::cout);
    std::cout << std::endl;

}
