/*!
 * \file dbg_log/dbg_log_t_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 *__legal_b__
 *
 *__legal_e__
 * \endcond
 *
 */

// dbg_log_t.cpp : source file that includes just the standard includes
// dbg_log_t.pch will be the pre-compiled header
// dbg_log_t.obj will contain the pre-compiled type information

#include "dbg_log_t/dbg_log_t_prec.h"

// TODO: reference any additional headers you need in dbg_log_t_prec.h
// and not in this file

