/*!
 * \file logmod/logmod_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 *
 */

// logger.cpp : source file that includes just the standard includes
// logger.pch will be the pre-compiled header
// logger.obj will contain the pre-compiled type information

#include "logmod/logmod_prec.h"

// TODO: reference any additional headers you need in logger_prec.h
// and not in this file

