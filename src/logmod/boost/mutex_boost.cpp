/*!
 * \file logmod/boost/mutex.cpp
 * \brief Mutex implemented with Boost
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016-2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#include "logmod/logmod_prec.h"
#include "logmod/boost/mutex.h"
#include "logmod/boost/muteximpl.h"

namespace logmod
{

namespace boost
{

mutex::mutex() : logger_mutex_api()
{
    m_impl = new muteximpl(this);
}

mutex::~mutex()
{
    delete m_impl;
}

int mutex::Lock()
{
    assert(m_impl != nullptr);

    return m_impl->Lock();
}

int mutex::Unlock()
{
    assert(m_impl != nullptr);

    return m_impl->Unlock();
}

muteximpl &mutex::GetImpl()
{
    assert(m_impl != nullptr);

    return *m_impl;
}

mutex_locker::mutex_locker(::logmod::boost::mutex &mux)
{
    m_impl = new mutex_lockerimpl(mux);
}

mutex_locker::~mutex_locker()
{
    delete m_impl;
}

}

}

