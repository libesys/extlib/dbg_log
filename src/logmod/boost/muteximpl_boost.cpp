/*!
 * \file logmod/boost/muteximpl_boost.cpp
 * \brief Mutex implemented with Boost
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#include "logmod/logmod_prec.h"
#include "logmod/boost/muteximpl.h"

namespace logmod
{

muteximpl::muteximpl(mutex * self)
{
    m_self = self;
}

muteximpl::~muteximpl()
{
}

int muteximpl::Lock()
{
    int result = 0;

    m_mutex.lock();
    return result;
}

int muteximpl::Unlock()
{
    int result = 0;

    m_mutex.unlock();
    return result;
}

::boost::recursive_mutex &muteximpl::int_mutex()
{
    return m_mutex;
}

/*mutex_lockerimpl::mutex_lockerimpl(::logmod::mutex &mux) : ::boost::recursive_mutex::scoped_lock(mux.int_mutex())
{
}

mutex_lockerimpl::~mutex_lockerimpl()
{
} */


}






