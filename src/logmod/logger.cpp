/*!
 * \file logmod/logger.cpp
 * \brief Defines some utilities to handle events
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#include "logmod/logmod_prec.h"
#include "logmod/logger.h"
#include "logmod/wxtextctrlts.h"

#ifdef WIN32
#include <sys/types.h>
#include <sys/timeb.h>
#else
#include <stdlib.h>
#include <sys/time.h>
#endif

#include <ios>

namespace logmod
{

int logger::m_time_mode=logger::TIME_OFF; //std::ios_base::xalloc();
const int logger::m_add_depth=std::ios_base::xalloc();
bool logger::m_old_way=true;
time_t logger::m_t_start=0;
#ifdef WIN32
_timeb logger::m_tb_start;
#endif


void logger::SetTimeMode(int mode)
{
    m_time_mode=mode;
}

int logger::GetTimeMode()
{
    return m_time_mode;
}

void logger::ResetTime()
{
    switch (m_time_mode)
    {
        /*case LOG_SYSTEMC:
            break; */
        case TIME_DFLT:
            ::time(&m_t_start);
#ifdef WIN32
            _ftime(&m_tb_start);
#endif
            //TODO
            break;
    }
}

logger::logger()
{
#ifdef LOGGER_WITH_MUTEX
    m_transaction_mutex = new mutex();
#else
    m_transaction_mutex = nullptr;
#endif
}

logger::~logger()
{
    if (m_transaction_mutex != nullptr)
        delete m_transaction_mutex;
}

#ifdef DBG_LOG_WX_USE

void logger::SetTextCtrl(wxTextCtrlTS *text)
{
    if (m_ofs!=NULL)
        SetOfstream(NULL);

    m_text_ctrl=text;
    if (text!=NULL)
    {
        if (m_os==NULL)
            m_os=new std::ostream(text);
        else
            m_os->rdbuf(text);
    }
    else
    {
        if (m_os!=NULL)
        {
            delete m_os;
            m_os=NULL;
        }
    }
}

wxTextCtrlTS *logger::GetTextCtrl()
{
    return m_text_ctrl;
}

#endif

void logger::SetOfstream(std::ofstream *ofs)
{
#ifdef DBG_LOG_WX_USE
    if (m_text_ctrl!=NULL)
        SetTextCtrl(NULL);
#endif

    m_os=(std::ostream *)ofs;
    m_ofs=ofs;
}

std::ofstream *logger::GetOfstream()
{
    return m_ofs;
}

void logger::SetOstream(std::ostream *os)
{
    m_os=os;
}

std::ostream *logger::GetOstream()
{
    return m_os;
}

void logger::Begin()
{
    if (m_transaction_mutex!=nullptr)
    m_transaction_mutex->Lock();
}

void logger::End()
{
    if (m_transaction_mutex != nullptr)
    m_transaction_mutex->Unlock();
}

void logger::Flush()
{
    if (m_os != nullptr)
        m_os->flush();
}

logger &begin(logger &mylog)
{
    mylog.Begin();

    return mylog;
}

logger &end(logger &mylog)
{
    mylog.End();

    return mylog;
}

logger &time(logger &mylog)
{
    switch (logger::GetTimeMode())
    {
        /*case logger::LOG_SYSTEMC:
            time = sc_time_stamp();
            mylog << "[" << time << "]";
            break; */
        case logger::TIME_DFLT:
            ::time(&mylog.m_t_end);
#ifdef WIN32
            _ftime(&mylog.m_tb_end);
            uint32_t __time;
            __time=(mylog.m_tb_end.time-logger::m_tb_start.time)*1000+mylog.m_tb_end.millitm-logger::m_tb_start.millitm;
            mylog << "[" << std::dec << __time << "ms]";
#endif

            break;
    }
    return mylog;
}

// Operator overloads

/*! \fn logger& logger::operator<<(ostream &os)
 *
 * operator<< definition for logger class. The input parameter is
 * printed out through ostream << if m_must_log flag is set.

 * \param[in] ostream &os Stream to be printed to m_ofs.
 */
logger& logger::operator<< (std::ostream &os)
{
#ifdef LOGGER_WITH_MUTEX
    mutex_locker lock(*m_transaction_mutex);
#endif
    //if (m_must_log == true)
    if (this->m_os!=NULL)
    {
        //*(this->m_os) << os; msvc14 doesn't like this
    }
    return *this;
}

logger& logger::operator<< (std::ostream& (*op)(std::ostream&))
{
#ifdef LOGGER_WITH_MUTEX
    mutex_locker lock(*m_transaction_mutex);
#endif
    /*int aaa,size;
    logger *_llog;
    std::ostream *_os;

    if (m_must_log == true)
    {
        if (m_own_ofs!=NULL)
            (*op)(*(this->m_own_ofs));
        else
            (*op)(*(this->m_ofs));

        size=this->m_borrow_logger.size();

        for (aaa=0;aaa<size;aaa++)
        {
            _llog=this->m_borrow_logger.at(aaa);

            if (_llog!=NULL)
                (*_llog) << op;
        }

        size=m_borrow_os.size();

        for (aaa=0;aaa<size;aaa++)
        {
            _os=m_borrow_os.at(aaa);

            if (_os!=NULL)
                (*_os) << op;       //ATTENTION: quite dangerous because of loop which could be created!!!
        }
    } */
    return *this;
}

logger& logger::operator<< (logger& (*op)(logger& os))
{
#ifdef LOGGER_WITH_MUTEX
    mutex_locker lock(*m_transaction_mutex);
#endif

    return (*op)(*(this));
}

void logger::SetDepth(int depth)
{
    m_depth=depth;
}

void logger::SetDepthSize(int size)
{
    m_depth_size=size;
}

void logger::SetStaticHeaderSize(int size)
{
    m_static_header_size=size;
}

void logger::SetStaticFooterSize(int size)
{
    m_static_footer_size=size;
}

void logger::HeaderEnable(bool enable)
{
    m_enable_header=enable;
}

bool logger::GetHeaderEnable()
{
    return m_enable_header;
}

void logger::AddDepth(std::string &str,bool with_static_header)
{
    std::string src(str),dst,depth;
    std::string::size_type i=0;

    if (with_static_header==true)
        //dst.Append(' ',m_static_header_size);
        dst.append(m_static_header_size,' ');
    depth.append("\n");
    //depth.Append("\n");
    //depth.Append(' ',m_depth*m_depth_size);
    depth.append(m_depth*m_depth_size,' ');

    //src.Replace("\n",depth);
    while ((i=src.find("\n",i))!=src.npos)
    {
        src.replace(i,1,depth);
        i++;
    }

    //dst.Append(src);
    dst.append(src);
    src=dst;
    //str.assign(dst.GetData().AsChar());
}

}
