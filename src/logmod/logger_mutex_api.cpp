/*!
 * \file logmod/logger_mutex_api.cpp
 * \brief 
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 *  \endcond
 */

#include "logmod/logmod_prec.h"
#include "logmod/logger_mutex_api.h"

namespace logmod
{

logger_mutex_api::~logger_mutex_api()
{
}

}




