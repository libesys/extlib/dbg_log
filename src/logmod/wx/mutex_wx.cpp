/*!
 * \file logmod/wx/mutex.cpp
 * \brief Mutex implemented with wxWidgets
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2016 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 */

#include "logmod/logmod_prec.h"
#include "logmod/wx/mutex.h"

#ifdef DBG_LOG_WX_USE

namespace logmod
{

mutex::mutex() : logger_mutex_api(), m_mutex(wxMUTEX_RECURSIVE)
{	
}

mutex::~mutex()
{
}

int mutex::Lock()
{
	int result = 0;

	result = m_mutex.Lock();
	return result;
}

int mutex::Unlock()
{
	int result = 0;

	result = m_mutex.Unlock();
	return result;
}

wxMutex &mutex::int_mutex()
{
	return m_mutex;
}

}

}

#endif

