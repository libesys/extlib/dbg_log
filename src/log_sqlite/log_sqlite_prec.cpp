/*!
 * \file log_sqlite/log_sqlite_prec.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 *
 */

// log_sqlite.cpp : source file that includes just the standard includes
// log_sqlite.pch will be the pre-compiled header
// log_sqlite.obj will contain the pre-compiled type information

#include "log_sqlite/log_sqlite_prec.h"

// TODO: reference any additional headers you need in log_sqlite_prec.h
// and not in this file

