/*!
 * \file dbg_log/dbg_class_thread.cpp
 * \brief Defines the event used by the dbg_class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#include "dbg_log/dbg_log_prec.h"

#include "dbg_log/dbg_class_thread.h"

namespace dbg_log
{

uint32_t dbg_class_thread::g_count = 0;

uint32_t dbg_class_thread::GetCount()
{
    return g_count;
}

dbg_class_thread::dbg_class_thread(): m_depth(0)
{
    m_id = g_count;
    ++g_count;
}

dbg_class_thread::~dbg_class_thread()
{
}

void dbg_class_thread::SetName(const std::string &name)
{
    m_name=name;
}

const std::string &dbg_class_thread::GetName()
{
    return m_name;
}

void dbg_class_thread::SetId(uint32_t id)
{
    m_id=id;
}

uint32_t dbg_class_thread::GetId()
{
    return m_id;
}

int dbg_class_thread::GetDepth()
{
    return m_depth;
}

void dbg_class_thread::SetDepth(int depth)
{
    m_depth=depth;
}

void dbg_class_thread::IncDepth()
{
    m_depth++;
}

void dbg_class_thread::DecDepth()
{
    m_depth--;
}

void dbg_class_thread::SetNativeId(NativeIdType native_id)
{
    m_native_id = native_id;
}

}

