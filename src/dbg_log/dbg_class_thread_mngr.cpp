/*!
 * \file dbg_log/dbg_class_thread_mngr.cpp
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#include "dbg_log/dbg_log_prec.h"
#include "dbg_log/dbg_class_thread_mngr.h"
#include "dbg_log/dbg_class_thread.h"

namespace dbg_log
{

//uint32_t dbg_class_thread_mngr::g_count = 0;
std::map<dbg_class_thread_mngr::NativeIdType, dbg_class_thread *> dbg_class_thread_mngr::g_named_threads;
std::vector<dbg_class_thread *> dbg_class_thread_mngr::g_list_threads;
logmod::mutex dbg_class_thread_mngr::g_mutex;
dbg_class_thread_mngr dbg_class_thread_mngr::g_mngr;

dbg_class_thread_mngr::NativeIdType dbg_class_thread_mngr::GetCuttentNativeId()
{
#ifdef DBG_LOG_WX_USE
    return wxThread::GetCurrentId();
#elif defined(DBG_LOG_BOOST_USE)
    return boost::this_thread::get_id();
#endif
}

dbg_class_thread *dbg_class_thread_mngr::GetCurrent()
{
    std::map<NativeIdType, dbg_class_thread *>::iterator it;
    NativeIdType id = dbg_class_thread_mngr::GetCuttentNativeId();
    dbg_class_thread *result;

    g_mutex.Lock();

    it = g_named_threads.find(id);

    if (it == g_named_threads.end())
        result = nullptr;
    else
        result = it->second;

    g_mutex.Unlock();
    return result;
}

dbg_class_thread *dbg_class_thread_mngr::AddCurrent(const std::string &name)
{
    dbg_class_thread *cur_thread = new dbg_class_thread();
    NativeIdType id = dbg_class_thread_mngr::GetCuttentNativeId();

    cur_thread->SetName(name);
    g_mutex.Lock();
    g_list_threads.push_back(cur_thread);
    g_named_threads[id] = cur_thread;
    g_mutex.Unlock();
    return cur_thread;
}

void dbg_class_thread_mngr::SetCurrentName(const std::string &name)
{
    dbg_class_thread *cur_thread = nullptr;
    std::map<NativeIdType, dbg_class_thread *>::iterator it;
    NativeIdType id = dbg_class_thread_mngr::GetCuttentNativeId();

    it = g_named_threads.find(id);
    if (it == g_named_threads.end())
    {
        cur_thread = new dbg_class_thread();
        g_mutex.Lock();
        g_list_threads.push_back(cur_thread);
        g_named_threads[id] = cur_thread;
        g_mutex.Unlock();
    }
    else
        cur_thread = it->second;

    cur_thread->SetName(name);
}

uint32_t dbg_class_thread_mngr::GetCount()
{
    return dbg_class_thread::GetCount();
}

dbg_class_thread_mngr::dbg_class_thread_mngr()
{
}

dbg_class_thread_mngr::~dbg_class_thread_mngr()
{
    std::vector<dbg_class_thread *>::iterator it;

    if (g_list_threads.size() == 0)
        return;

    for (it = g_list_threads.begin(); it != g_list_threads.end(); ++it)
    {
        if (*it != nullptr)
        {
            delete *it;
            *it = nullptr;
        }
    }
    g_list_threads.clear();
    g_named_threads.clear();
}

}

