/*!
 * \file dbg_log/dbg_class_call_helper.cpp
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#include "dbg_log/dbg_log_prec.h"
#include "dbg_log/dbg_class_call_helper.h"

#include <boost/tokenizer.hpp>

namespace dbg_log
{

dbg_class_call_helper::dbg_class_call_helper(const char *s, const char *var_list)
    : m_prof(s)
    , m_info(s)
    , m_var_list(var_list ? var_list : "")
{
    if (var_list != nullptr)
    {
        typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
        boost::char_separator<char> sep(", ");
        tokenizer tokens(m_var_list, sep);
        tokenizer::iterator it;

        for (it = tokens.begin(); it != tokens.end(); ++it)
        {
            m_names.push_back(*it);
        }
    }
}

}
