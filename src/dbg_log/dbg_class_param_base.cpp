/*!
 * \file dbg_log/dbg_class_param_base.cpp
 * \brief Defines the event used by the dbg_class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */


#include "dbg_log/dbg_log_prec.h"

#include "dbg_log/dbg_class_param_base.h"

#ifdef DBG_LOG_WX_USE
#include <wx/string.h>
#elif defined DBG_LOG_BOOST_USE
#include <boost/locale.hpp>
#endif

namespace dbg_log
{

dbg_class_param_base::dbg_class_param_base(char *name,bool output)
    : m_name(name), m_output_param(output), m_is_array(false), m_array_len(0), m_dbg_class(0)
{}

dbg_class_param_base::dbg_class_param_base(const char *name,bool output)
    : m_name(name), m_output_param(output), m_is_array(false), m_array_len(0), m_dbg_class(0)
{}

dbg_class_param_base::dbg_class_param_base(char *name,int len,bool output)
    : m_name(name), m_output_param(output), m_is_array(true), m_array_len(len), m_dbg_class(0)
{}

dbg_class_param_base::~dbg_class_param_base()
{}

void dbg_class_param_base::SetName(const char *name)
{
    m_name = name;
}

/*void dbg_class_param_base::Print(logmod::logger &os,bool enters)
{} */

bool dbg_class_param_base::IsOutput()
{
    return m_output_param;
}

void dbg_class_param_base::SetOuput(bool value)
{
    m_output_param=value;
}

void dbg_class_param_base::SetDbgClass(dbg_class *dbgclass)
{
    m_dbg_class=dbgclass;
}

dbg_class *dbg_class_param_base::GetDbgClass()
{
    return m_dbg_class;
}

void dbg_class_param_base::StrConvert(std::string &v,const std::wstring &value)
{
#ifdef DBG_LOG_WX_USE
    wxString wx_str;

    wx_str=value;

    v=wx_str.c_str().AsChar();
#elif defined DBG_LOG_BOOST_USE
    v=boost::locale::conv::utf_to_utf<char>(value);
#else
#endif
}

void dbg_class_param_base::StrConvert(std::string &v, std::wstring &value)
{
#ifdef DBG_LOG_WX_USE
    wxString wx_str;

    wx_str=value;

    v=wx_str.c_str().AsChar();
#elif defined DBG_LOG_BOOST_USE
    v = boost::locale::conv::utf_to_utf<char>(value);
#else
#endif
}

}
