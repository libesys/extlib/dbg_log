/*!
 * \file dbg_log/dbg_class_prof_bug.cpp
 * \brief Defines the event used by the dbg_class
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 */


#include "dbg_log/dbg_log_prec.h"

#include "dbg_log/dbg_class_prof_bug.h"

namespace dbg_log
{

dbg_class_prof_bug::dbg_class_prof_bug(dbg_class_prof *prof)
{
    m_prof=prof;
}

dbg_class_prof_bug::~dbg_class_prof_bug()
{
    m_prof->End();
}

}
