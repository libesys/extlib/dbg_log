/*!
 * \file dbg_log/dbg_class_thread_mngr.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#ifndef __DBG_LOG_DBG_CLASS_THREAD_MNGR_H__
#define __DBG_LOG_DBG_CLASS_THREAD_MNGR_H__

#include "dbg_log/dbg_log_defs.h"
#include "dbg_log/inttypes.h"

#include <logmod/mutex.h>

#include <string>
#include <map>
#include <vector>

#ifdef DBG_LOG_BOOST_USE
#include <boost/thread.hpp>
#endif

namespace dbg_log
{

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

class DBG_LOG_API dbg_class_thread;

class DBG_LOG_API dbg_class_thread_mngr
{
public:
#ifdef DBG_LOG_WX_USE
    typedef uint64_t NativeIdType;
#elif defined(DBG_LOG_BOOST_USE)
    typedef ::boost::thread::id NativeIdType;
#endif

    dbg_class_thread_mngr();
    virtual ~dbg_class_thread_mngr();

    static dbg_class_thread *GetCurrent();
    static dbg_class_thread *AddCurrent(const std::string &name);
    static uint32_t GetCount();
    static void SetCurrentName(const std::string &name);
protected:
    static dbg_class_thread_mngr g_mngr;
    static std::map<NativeIdType,dbg_class_thread *> g_named_threads;
    static std::vector<dbg_class_thread *> g_list_threads;
    static NativeIdType GetCuttentNativeId();
    static logmod::mutex g_mutex;
};

#ifdef _MSC_VER
#pragma warning(pop)
#endif

}

#endif

