/*!
 * \file dbg_log/dbg_class_param_array.h
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __DBG_LOG_DBG_CLASS_PARAM_ARRAY_H__
#define __DBG_LOG_DBG_CLASS_PARAM_ARRAY_H__

#include <dbg_log/dbg_log_defs.h>
#include <dbg_log/inttypes.h>
#include <dbg_log/dbg_class_param_base.h>
#include <dbg_log/dbg_class.h>
#include <logmod/logger.h>

#include <ostream>
#include <iomanip>
#include <vector>

namespace dbg_log
{

template<typename OS, typename T>
class dbg_class_param_print
{
public:
    void Print(OS &os, T &val)
    {
        os << val;
    }

};

template<typename OS>
class dbg_class_param_print<OS,uint8_t>
{
public:
    void Print(OS &os, uint8_t &val)
    {
        int value=val;

        os << "0x" << std::hex << std::setw(2) << std::setfill('0') << value;
    }

};

/*! \class dbg_class_param_array dbg_log/dbg_class.h "dbg_log/dbg_class.h"
 *  \brief Template class for handling a paramter of type T
 */
template<typename T, typename SIZE>
class dbg_class_param_array: public dbg_class_param_base
{
public:
    //dbg_class_param_array(char *name,T *value, SIZE &size, bool output=false);     //!< Constructor
    dbg_class_param_array(const char *name, T *value, SIZE &size, bool output = false);     //!< Constructor
    dbg_class_param_array(const char *name, T *value, SIZE &size, SIZE print_size, bool output = false);     //!< Constructor
    virtual ~dbg_class_param_array();                                 //!< Destructor

    virtual void Print(logmod::logger &os,bool enters=true);    //!< Print the parameter
    virtual void Print(std::ostream &os,bool enters=true);      //!< Print the parameter

    void SetPrintSize(SIZE print_size);
    SIZE GetPrintSize();
    template<typename OS> void Print_t(OS &os, bool enters=true);

    T *m_value;                                                 //!< The actual parameter
    SIZE &m_size;                                               //!< The size of the array
    SIZE m_print_size;                                          //!< The size to be printed
};

/*template<typename T, typename SIZE>
dbg_class_param_array<T,SIZE>::dbg_class_param_array(char *name,T *value,SIZE &size,bool output)
    :dbg_class_param_base(name,output),m_value(value),m_size(size)
{} */

template<typename T, typename SIZE>
dbg_class_param_array<T,SIZE>::dbg_class_param_array(const char *name,T *value,SIZE &size,bool output)
    : dbg_class_param_base(name,output),m_value(value),m_size(size), m_print_size(0)
{
}

template<typename T, typename SIZE>
dbg_class_param_array<T, SIZE>::dbg_class_param_array(const char *name, T *value, SIZE &size, SIZE print_size, bool output)
    : dbg_class_param_base(name, output), m_value(value), m_size(size), m_print_size(print_size)
{
}

template<typename T, typename SIZE>
dbg_class_param_array<T,SIZE>::~dbg_class_param_array()
{}

template<typename T, typename SIZE>
void dbg_class_param_array<T,SIZE>::Print(logmod::logger &os,bool enters)
{
    Print_t<logmod::logger>(os,enters);
}

template<typename T, typename SIZE>
void dbg_class_param_array<T,SIZE>::Print(std::ostream &os,bool enters)
{
    Print_t<std::ostream>(os,enters);
}

template<typename T, typename SIZE>
void dbg_class_param_array<T, SIZE>::SetPrintSize(SIZE print_size)
{
    m_print_size = print_size;
}

template<typename T, typename SIZE>
SIZE dbg_class_param_array<T, SIZE>::GetPrintSize()
{
    return m_print_size;
}

template<typename T, typename SIZE>
template<typename OS>
void dbg_class_param_array<T,SIZE>::Print_t(OS &os, bool enters)
{
    SIZE idx;
    SIZE size;
    int int_size;
    bool full_print=false;
    dbg_class_param_print<OS,T> printer;

    if (enters)
    {
        if (!IsOutput())
        {
            full_print=true;
        }
    }
    else
    {
        if (IsOutput())
        {
            full_print=true;
        }
    }

    if (full_print)
    {
        os << m_name << "[" << m_size << "] = {" << std::endl;

        size = m_size;
        int_size = dbg_class::GetArrayPrintSize();
        if ((int_size > 0) && (int_size < size))
            size = (SIZE)int_size;
        if ((m_print_size != 0) && (m_print_size < size))
            size = m_print_size;

        for (idx=0; idx<size; ++idx)
        {
            if ((idx % 16 == 0) && (idx != 0))
                os << std::endl;
            //os << "0x" << std::hex << std::setw(2) << std::setfill('0') << m_value[idx] << " ";
            //PrintU_t<OS,T>(os,&m_value[idx]);
            printer.Print(os,m_value[idx]);
            os << " ";
        }
        if (size != m_size)
            os << " ...";
        if (idx%16!=0)
            os << std::endl;
        os << "   }" << std::endl;
    }
    else
    {
        os << m_name << " = [addr] " << (void *)m_value << std::endl;
    }
}

template<typename T, typename SIZE>
class dbg_class_param_array<T*,SIZE&>: public dbg_class_param_array<T,SIZE>
{
public:
    //dbg_class_param_array(char *name,T *value, SIZE &size, bool output=false);     //!< Constructor
    dbg_class_param_array(const char *name,T *value, SIZE &size, bool output=false);     //!< Constructor
    virtual ~dbg_class_param_array();                                 //!< Destructor

};

/*template<typename T, typename SIZE>
dbg_class_param_array<T*, SIZE&>::dbg_class_param_array(char *name,T *value,SIZE &size,bool output)
    : dbg_class_param_array<T*,SIZE>(name,value,size,output)
{} */

template<typename T, typename SIZE>
dbg_class_param_array<T*, SIZE&>::dbg_class_param_array(const char *name,T *value,SIZE &size,bool output)
    : dbg_class_param_array<T,SIZE>(name,value,size,output)
{}

template<typename T, typename SIZE>
dbg_class_param_array<T*, SIZE&>::~dbg_class_param_array()
{}

template<typename T, typename SIZE>
class dbg_class_param_array<T*,SIZE>: public dbg_class_param_array<T,SIZE>
{
public:
    //dbg_class_param_array(char *name,T *value, SIZE &size, bool output=false);     //!< Constructor
    dbg_class_param_array(const char *name,T *value, SIZE &size, bool output=false);     //!< Constructor
    virtual ~dbg_class_param_array();                                 //!< Destructor

};

/*template<typename T, typename SIZE>
dbg_class_param_array<T*, SIZE&>::dbg_class_param_array(char *name,T *value,SIZE &size,bool output)
    : dbg_class_param_array<T*,SIZE>(name,value,size,output)
{} */

template<typename T, typename SIZE>
dbg_class_param_array<T*, SIZE>::dbg_class_param_array(const char *name,T *value,SIZE &size,bool output)
    : dbg_class_param_array<T,SIZE>(name,value,size,output)
{}

template<typename T, typename SIZE>
dbg_class_param_array<T*, SIZE>::~dbg_class_param_array()
{}

}

#endif
