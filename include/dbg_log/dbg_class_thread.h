/*!
 * \file dbg_log/dbg_class_thread.h
 * \brief Defines the event used by the dbg_class
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#ifndef __DBG_LOG_DBG_CLASS_THREAD_H__
#define __DBG_LOG_DBG_CLASS_THREAD_H__

#include "dbg_log/dbg_log_defs.h"
#include "dbg_log/inttypes.h"

#include <logmod/mutex.h>

#include <string>

#ifdef DBG_LOG_BOOST_USE
#include <boost/thread.hpp>
#endif

namespace dbg_log
{

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

class DBG_LOG_API dbg_class;

class DBG_LOG_API dbg_class_thread
{
public:
    friend class DBG_LOG_API dbg_class;
#ifdef DBG_LOG_WX_USE
    typedef uint64_t NativeIdType;
#elif defined(DBG_LOG_BOOST_USE)
    typedef ::boost::thread::id NativeIdType;
#endif
    dbg_class_thread();
    virtual ~dbg_class_thread();

    void SetName(const std::string &name);
    const std::string &GetName();

    void SetId(uint32_t id);
    uint32_t GetId();

    int GetDepth();
    void SetDepth(int depth);
    void IncDepth();
    void DecDepth();

    static uint32_t GetCount();
protected:
    void SetNativeId(NativeIdType native_id);

    static uint32_t g_count;

    std::string m_name;
    int m_depth;
    uint32_t m_id;
    NativeIdType m_native_id;
};

#ifdef _MSC_VER
#pragma warning(pop)
#endif

}

#endif
