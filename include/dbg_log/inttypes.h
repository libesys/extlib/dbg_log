/*!
 * \file dbg_log/inttypes.h
 * \brief Defines the event used by the dbg_class
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 */

#ifndef __DBG_LOG_INTTYPES_H__
#define __DBG_LOG_INTTYPES_H__

#include <logmod/inttypes.h>

#endif
