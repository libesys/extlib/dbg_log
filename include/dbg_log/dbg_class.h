/*!
 * \file dbg_log/dbg_class.h
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2017 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#ifndef __DBG_CLASS_H__
#define __DBG_CLASS_H__

#ifdef DBG_LOG

#include "dbg_log/dbg_log_defs.h"
#include "dbg_log/inttypes.h"

#include "dbg_log/dbg_class_prof.h"
#include "dbg_log/dbg_class_prof_bug.h"
#include "dbg_log/dbg_class_function_info.h"
#include "dbg_log/dbg_class_class_info.h"
#include "dbg_log/dbg_class_param_base.h"
#include "dbg_log/dbg_class_param.h"
#include "dbg_log/dbg_class_call_helper.h"
//#include "dbg_log/dbg_class_param_array.h"
#include "dbg_log/dbg_class_event.h"
#include "dbg_log/dbg_class_thread_mngr.h"
#include "logmod/event_logger.h"

#include "logmod/logger.h"

#include <ostream>
#include <vector>
#include <sstream>
#include <map>

#ifdef DBG_LOG_BOOST_USE
#include <boost/thread.hpp>
#endif

namespace dbg_log
{

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif

class DBG_LOG_API dbg_class
{
public:
    dbg_class(const char *name,bool print_state);
    dbg_class(dbg_class *dbg);
    virtual ~dbg_class();

    void Message(const char *msg);                  //!< Send a message
    void Message(const std::string &msg);                  //!< Send a message
    void MessageF(const char *format,...);                  //!< Send a message

    void SetFunctionInfo(dbg_class_function_info *info);

    virtual int GetDepth();             //!< Give the calling depth
    void IncDepth(dbg_class_thread *thread_info=NULL);	//!< Increment by one the calling depth
    void DecDepth(dbg_class_thread *thread_info=NULL);	//!< Decrement by one the calling depth
    static void ResetDepth();           //!< Set the depth to zero
    static int Depth(dbg_class_thread **thread_info=NULL);             //!< Give the calling depth
    void AllParamAdded();                           //!< All parameters where added
    void AddParam(dbg_class_param_base *param);     //!< Add a parameter to be traced
    void SetReturn(dbg_class_param_base *ret);      //!< Set the return value to be traced
    void PrintBlankHeader(std::ostream &os);
    void PrintRegId(std::ostream &os, int id);
    void PrintParamHeader(std::ostream &os);
    void PrintReturnHeader(std::ostream &os);
    void PrintCurTime(std::ostream &os);
    void PrintDepth(std::ostream &os);
    void PrintThreadName(std::ostream &os);
    void AddDepth(std::string &str,bool with_static_header);

    static void SetDifferentiateThread(bool value);
    static uint64_t GetCurTime();
    static void SetDefaultLogger(logmod::logger *logger);
    static logmod::logger *GetDefaultLogger();
    static void SetDefaultEventLogger(logmod::event_logger *log);  //!< Set the default event logger

    static void SetThreadNameWidth(int size);
    static int GetThreadNameWidth();
    static void Enable(bool enable=true);
    static void Disable();

    static void Flush();

    static void SetArrayPrintSize(int array_print_size);
    static int GetArrayPrintSize();

    inline std::ostringstream &GetTempOss(bool clear=false)
    {
        if (clear)
            m_os_temp.str("");
        return m_os_temp;
    }
protected:
    static bool m_enable;
    static int m_depth;                             //!< Overall calling depth
    static bool m_diff_thread;
    static int m_thread_name_width;
    static std::string m_depth_str;
    static std::string m_blank_hdr;
    static std::string m_param_hdr;
    static std::string m_blank_param_hdr;
    static std::string m_struct_param_hdr;
    static std::string m_return_hdr;
    int m_my_depth;                                 //!< Local calling depth
    //static wxTextCtrl *m_text;
    bool m_end = false;                             //!< True if the last in the stack
    dbg_class *m_dbg_class = nullptr;               //!< Pointer to the first dbg_class on the stack
    dbg_class_function_info *m_fct_info = nullptr;  //!< The class holding info about the function being called
    std::vector<dbg_class_param_base *> m_params;   //!< Vector of parameters
    dbg_class_param_base *m_return = nullptr;       //!< The return value
    std::ostringstream m_os_str;                    //!< Output string stream
    std::ostringstream m_os_temp;                    //!< Output string stream
    const char *m_name = nullptr;
    bool m_print_state = false;                      //!< If true, the state of the module will be logged
    logmod::logger *m_logger = nullptr;             //!< The logger used to ouput traces
    static logmod::logger *m_dft_logger;            //!< The default logger
    static logmod::event_logger *m_dft_event_logger;//!< The default event logger;
    dbg_class_event m_event;                        //!< Event which can be logged

    dbg_class_thread *m_thread_info = nullptr;
    static int m_array_print_size;
};

template<typename T>
dbg_class_param<T> create_dbg_class_param(char *name, T &test, bool output = false)
{
    dbg_class_param<T> s_param(name, test, output);

    return s_param;
}

#ifdef _MSC_VER
#pragma warning(pop)
#endif

}

#endif

#ifdef DBG_LOG

#define DBG_DEFINE_CLASS(type) \
    static dbg_log::dbg_class_class_info m_dbg_class_class_info_##type; \
    virtual dbg_log::dbg_class_class_info *__get_dbg_class_class_info() \
    { \
        return &m_dbg_class_class_info_##type; \
    }

#define DBG_DECLARE_CLASS(type,name) \
    dbg_log::dbg_class_class_info type::m_dbg_class_class_info_##type(name);

#define DBG_CALLMEMBER(name,print_state) \
    static dbg_log::dbg_class_prof __up_dbg_class_member_call_prof(name); \
    static dbg_log::dbg_class_function_info __up_dbg_member_call_info(name); \
    dbg_log::dbg_class __up_dbg_class_member_call(name,print_state); \
    __up_dbg_class_member_call.SetFunctionInfo(&__up_dbg_member_call_info); \
    __up_dbg_class_member_call_prof.IncCall(); \
    __up_dbg_class_member_call_prof.Start(); \
    dbg_log::dbg_class_prof_bug __up_dbg_class_member_call_prof_bug(&__up_dbg_class_member_call_prof)

#if defined(_MSC_VER)
#define DBG_CALL_BEGIN(print_state) \
	static dbg_log::dbg_class_call_helper __up_dbg_class_member_call_test(__FUNCSIG__); \
    dbg_log::dbg_class __up_dbg_class_member_call(__FUNCSIG__, print_state); \
    __up_dbg_class_member_call.SetFunctionInfo(&__up_dbg_class_member_call_test.m_info); \
    __up_dbg_class_member_call_test.m_prof.IncCall(); \
    __up_dbg_class_member_call_test.m_prof.Start(); \
    dbg_log::dbg_class_prof_bug __up_dbg_class_member_call_prof_bug(&__up_dbg_class_member_call_test.m_prof)
#elif defined(__GNUC__)
#define DBG_CALL_BEGIN(print_state) \
    static dbg_log::dbg_class_call_helper __up_dbg_class_member_call_test(__PRETTY_FUNCTION__); \
    dbg_log::dbg_class __up_dbg_class_member_call(__PRETTY_FUNCTION__, print_state); \
    __up_dbg_class_member_call.SetFunctionInfo(&__up_dbg_class_member_call_test.m_info); \
    __up_dbg_class_member_call_test.m_prof.IncCall(); \
    __up_dbg_class_member_call_test.m_prof.Start(); \
    dbg_log::dbg_class_prof_bug __up_dbg_class_member_call_prof_bug(&__up_dbg_class_member_call_test.m_prof)
#endif

#define DBG_CALLMEMBER_END \
    __up_dbg_class_member_call.AllParamAdded(); \
    dbg_log::dbg_class __up_dbg_class_member_call_end(&__up_dbg_class_member_call);

#define DBG_CALL_END \
	__up_dbg_class_member_call.AllParamAdded(); \
	dbg_log::dbg_class __up_dbg_class_member_call_end(&__up_dbg_class_member_call);

//! Macro to trace of function with a return value
/* \param name the name of the member function
 * \param state true if the PrintState should be called, false if not
 * \param type the type of the return value
 * \param var the variable which will store the return value
 *
 */
#define DBG_CALLMEMBER_RET(name,state,type,var) \
    static dbg_log::dbg_class_prof __up_dbg_class_member_call_prof(name); \
    static dbg_log::dbg_class_function_info __up_dbg_member_call_info(name); \
    type var; \
    dbg_log::dbg_class_param<type> __up_dbg_class_param_ret(#var,var); \
    dbg_log::dbg_class __up_dbg_class_member_call(name,state); \
    __up_dbg_class_member_call.SetFunctionInfo(&__up_dbg_member_call_info); \
    __up_dbg_class_member_call.SetReturn(&__up_dbg_class_param_ret); \
    __up_dbg_class_member_call_prof.IncCall(); \
    __up_dbg_class_member_call_prof.Start(); \
    dbg_log::dbg_class_prof_bug __up_dbg_class_member_call_prof_bug(&__up_dbg_class_member_call_prof);

#define DBG_CALL_RET_DEF_BEGIN(print_state, type, var) \
	static dbg_log::dbg_class_call_helper __up_dbg_class_member_call_test(__FUNCSIG__); \
	type var; \
	dbg_log::dbg_class_param<type> __up_dbg_class_param_ret(#var, var); \
    dbg_log::dbg_class __up_dbg_class_member_call(__FUNCSIG__, print_state); \
    __up_dbg_class_member_call.SetFunctionInfo(&__up_dbg_class_member_call_test.m_info); \
	__up_dbg_class_member_call.SetReturn(&__up_dbg_class_param_ret); \
    __up_dbg_class_member_call_test.m_prof.IncCall(); \
    __up_dbg_class_member_call_test.m_prof.Start(); \
    dbg_log::dbg_class_prof_bug __up_dbg_class_member_call_prof_bug(&__up_dbg_class_member_call_test.m_prof)

#define DBG_CALLMEMBER_RETREF(name,state,type,var_name,var) \
    static dbg_log::dbg_class_prof __up_dbg_class_member_call_prof(name); \
    static dbg_log::dbg_class_function_info __up_dbg_member_call_info(name); \
    type &var_name=var; \
    dbg_log::dbg_class_param<type &> __up_dbg_class_param_ret(#var_name,var_name); \
    dbg_log::dbg_class __up_dbg_class_member_call(name,state); \
    __up_dbg_class_member_call.SetFunctionInfo(&__up_dbg_member_call_info); \
    __up_dbg_class_member_call.SetReturn(&__up_dbg_class_param_ret); \
    __up_dbg_class_member_call_prof.IncCall(); \
    __up_dbg_class_member_call_prof.Start(); \
    dbg_log::dbg_class_prof_bug __up_dbg_class_member_call_prof_bug(&__up_dbg_class_member_call_prof);

#define DBG_PARAM(number,name,type,var) \
    dbg_log::dbg_class_param<type> __up_dbg_class_param##number(name,var); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##number);

#define DBG_P(name) \
    auto __up_dbg_class_param##name = dbg_log::create_dbg_class_param(#name, name); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##name);

#define DBG_NPARAM(name,type,var) \
    dbg_log::dbg_class_param<type> __up_dbg_class_param##name(name,var); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##name);

#define DBG_PARAM_OUT(number,name,type,var) \
    dbg_log::dbg_class_param<type> __up_dbg_class_param##number(name,var,true); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##number);

#define DBG_P_OUT(name) \
    auto __up_dbg_class_param##name = dbg_log::create_dbg_class_param(#name, name, true); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##name);

#define DBG_PARAM_ARRAY(number,name,type_var,var,type_size,size) \
    dbg_log::dbg_class_param_array<type_var,type_size> __up_dbg_class_param##number(name,var,size); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##number);

#define DBG_PARAM_ARRAY_PRINT_SIZE(number,name,type_var,var,type_size,size, print_size) \
    dbg_log::dbg_class_param_array<type_var,type_size> __up_dbg_class_param##number(name,var,size, print_size); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##number);

#define DBG_PARAM_ARRAY_OUT(number,name,type_var,var,type_size,size) \
    dbg_log::dbg_class_param_array<type_var,type_size> __up_dbg_class_param##number(name,var,size,true); \
    __up_dbg_class_member_call.AddParam(&__up_dbg_class_param##number);

#define DBG_MSG(msg) __up_dbg_class_member_call.Message(msg);
#define DBG_MSGF(format,...) __up_dbg_class_member_call.MessageF(format,__VA_ARGS__);

#define DBG_MSG_OS(msg) \
    __up_dbg_class_member_call.GetTempOss(true) << msg; \
    __up_dbg_class_member_call.Message(__up_dbg_class_member_call.GetTempOss().str());

#define DBG_SET_THREAD_NAME(name) \
    dbg_log::dbg_class_thread_mngr::SetCurrentName(name)

#define DBG_FLUSH \
    dbg_log::dbg_class::Flush()

#include "dbg_log/dbg_class_param_array.h"

#else

#define DBG_DEFINE_CLASS(type)

#define DBG_DECLARE_CLASS(type,name)

#define DBG_CALLMEMBER(name,print_state)


#define DBG_CALLMEMBER_END

#define DBG_CALLMEMBER_RET(name,state,type,var) \
    type var;

#define DBG_CALLMEMBER_RETREF(name,state,type,var_name,var)

#define DBG_PARAM(number,name,type,var)

#define DBG_NPARAM(name,type,var)

#define DBG_PARAM_OUT(number,name,type,var)

#define DBG_PARAM_ARRAY(number,name,type_var,var,type_size,size)

#define DBG_PARAM_ARRAY_OUT(number,name,type_var,var,type_size,size)

#define DBG_MSG(msg)
#define DBG_MSGF(format,...)

#define DBG_MSG_OS(msg)

#define DBG_SET_THREAD_NAME(name)

#define DBG_FLUSH

#endif

#endif
