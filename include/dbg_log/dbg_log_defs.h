/*!
 * \file dbg_log/dbg_log_defs.h
 * \brief Definitions needed for dbg_log
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2020 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifndef DBG_LOG_BOOST_USE
#define DBG_LOG_BOOST_USE
#endif

#ifdef DBG_LOG_EXPORTS
#define DBG_LOG_API __declspec(dllexport)
#elif DBG_LOG_USE
#define DBG_LOG_API __declspec(dllimport)
#else
#define DBG_LOG_API
#endif

#ifdef LOGMOD_LINUX
#include "logmod/unix/setup.h"
#endif

#include "logmod/inttypes.h"

#include "dbg_log/autolink.h"

