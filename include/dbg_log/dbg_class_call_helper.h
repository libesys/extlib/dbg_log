/*!
 * \file dbg_log/dbg_class_call_helper.h
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014-2019 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#include "dbg_log/dbg_log_defs.h"
#include "dbg_log/dbg_class_prof.h"
#include "dbg_log/dbg_class_function_info.h"

#include <string>
#include <vector>

namespace dbg_log
{

class DBG_LOG_API dbg_class_call_helper
{
public:
    dbg_class_call_helper(const char *s, const char *var_list = nullptr);

    dbg_log::dbg_class_prof m_prof;
    dbg_log::dbg_class_function_info m_info;
    std::string m_var_list;
    std::vector<std::string> m_names;
};

}
