/*!
 * \file dbg_log/dbg_class_prof_bug.h
 * \brief Defines the event used by the dbg_class
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 */

#ifndef __DBG_CLASS_PROF_BUG_H__
#define __DBG_CLASS_PROF_BUG_H__

#include "dbg_log/dbg_log_defs.h"
#include "dbg_log/inttypes.h"

#include "dbg_log/dbg_class_prof.h"

namespace dbg_log
{

class DBG_LOG_API dbg_class_prof_bug
{
public:
    dbg_class_prof_bug(dbg_class_prof *prof);
    virtual ~dbg_class_prof_bug();

    dbg_class_prof *m_prof;
};

}

#endif
