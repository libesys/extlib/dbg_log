/*!
 * \file log_sqlite/log_sqlite_prec.h
 * \brief For precompiled headers
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 *
 */

// log_sqlite_prec.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once
#include <wx/wx.h>
#ifdef WIN32
#include <wx/msw/winundef.h>

    #define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
    // Windows Header Files:
    //#include <windows.h>
#endif

// TODO: reference additional headers your program requires here

