/*!
 * \file dbg_log/logger_defs.h
 * \brief Definitions needed for logger
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014-2020 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 *
 */

#pragma once

#ifndef DBG_LOG_BOOST_USE
#define DBG_LOG_BOOST_USE
#endif

#ifndef LOGGER_WITH_MUTEX
#define LOGGER_WITH_MUTEX
#endif

#ifdef LOGMOD_EXPORTS
#define LOGMOD_API __declspec(dllexport)
#elif LOGMOD_USE
#define LOGMOD_API __declspec(dllimport)
#else
#define LOGMOD_API
#endif

#ifdef LOGMOD_LINUX
#include "logmod/unix/setup.h"
#endif

#include "logmod/inttypes.h"

#include "logmod/autolink.h"

