/*!
 * \file logmod/boost/mutex.h
 * \brief Mutex implemented with Boost
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#ifndef __LOGGER_BOOST_MUTEX_H__
#define __LOGGER_BOOST_MUTEX_H__

#include "logmod/logmod_defs.h"
#include "logmod/logger_mutex_api.h"

namespace logmod
{

namespace boost
{

class LOGMOD_API muteximpl;

class LOGMOD_API mutex: public logger_mutex_api
{
public:
    mutex();
    virtual ~mutex();

    virtual int Lock();
    virtual int Unlock();

    muteximpl &GetImpl();
protected:
    muteximpl *m_impl = nullptr;
};

class LOGMOD_API mutex_lockerimpl;

class LOGMOD_API mutex_locker
{
public:
    mutex_locker(::logmod::boost::mutex &mux);

    virtual ~mutex_locker();
protected:
    mutex_lockerimpl *m_impl = nullptr;
};

}

#ifdef DBG_LOG_BOOST_USE
using namespace boost;
#endif

}

#endif


