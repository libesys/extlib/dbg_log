/*!
 * \file logmod/boost/muteximpl.h
 * \brief Mutex implemented with Boost
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2017 Michel Gillet
 * Distributed under the wxWindows Library License, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#ifndef __LOGGER_BOOST_MUTEXIMPL_H__
#define __LOGGER_BOOST_MUTEXIMPL_H__

#include "logmod/logmod_defs.h"
#include "logmod/boost/mutex.h"

#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>

namespace logmod
{

namespace boost
{

class LOGMOD_API muteximpl
{
public:
    muteximpl(mutex *self);
    virtual ~muteximpl();

    int Lock();
    int Unlock();

    ::boost::recursive_mutex &int_mutex();
protected:
    mutex *m_self = nullptr;
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable:4251)
#endif
    ::boost::recursive_mutex m_mutex;
#ifdef _MSC_VER
#pragma warning(pop)
#endif
};

class mutex_lockerimpl
{
public:
    mutex_lockerimpl(::logmod::boost::mutex &mux)
        : m_lock(mux.GetImpl().int_mutex())
    {
    }
    virtual ~mutex_lockerimpl()
    {
    }
protected:
    ::boost::recursive_mutex::scoped_lock m_lock;
};

}

}


#endif



