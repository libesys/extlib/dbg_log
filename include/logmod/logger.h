/*!
 * \file logmod/logger.h
 * \brief Defines some utilities to handle events
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#ifndef __LOGGER_LOGGER_H__
#define __LOGGER_LOGGER_H__

#include "logmod/logmod_defs.h"
#include "logmod/inttypes.h"

#include "logmod/mutex.h"

#include <ostream>
#include <sstream>

#include <time.h>
#ifdef WIN32
#include <sys/timeb.h>
#endif

namespace logmod
{

class LOGMOD_API wxTextCtrlTS;

class LOGMOD_API logger
{
public:
    template<class T> friend logger& operator<< (logger& os,T& val);
    template<class T> friend logger& operator<< (logger& os,const T& val);
    friend LOGMOD_API logger &time(logger &mylog);
    enum
    {
        TIME_OFF=0,
        TIME_DFLT
    };
    logger();
    virtual ~logger();

#ifdef DBG_LOG_WX_USE
    void SetTextCtrl(wxTextCtrlTS *text);
    wxTextCtrlTS *GetTextCtrl();
#endif

    void SetOfstream(std::ofstream *ofs);
    std::ofstream *GetOfstream();

    void SetOstream(std::ostream *os);
    std::ostream *GetOstream();

    virtual void Begin();
    virtual void End();

    inline logger_mutex_api *GetTransactionMutex()
    {
        return m_transaction_mutex;
    }

    logger& operator<< (std::ostream &os);
    logger& operator<< (std::ostream& (*op)(std::ostream&));
    logger& operator<< (logger& (*op)(logger& os));

    virtual void SetDepth(int depth);
    virtual void SetDepthSize(int size);
    virtual void SetStaticHeaderSize(int size);
    virtual void SetStaticFooterSize(int size);
    virtual void HeaderEnable(bool enable=true);
    virtual bool GetHeaderEnable();
    void AddDepth(std::string &str,bool with_static_header=false);

    void Flush();

    static void SetTimeMode(int mode);
    static int GetTimeMode();
    static void ResetTime();
protected:
    mutex *m_transaction_mutex = nullptr;
    static int m_time_mode;
    static const int m_add_depth;
    std::ostream *m_os = nullptr;
    std::ofstream *m_ofs = nullptr;
    std::streambuf *m_streambuf = nullptr;
    int m_depth = 0;
    int m_depth_size = 0;
    int m_static_header_size = 0;
    int m_static_footer_size = 0;
    bool m_enable_header;
    static bool m_old_way;
    static time_t m_t_start;
    time_t m_t_end;
#ifdef WIN32
    static _timeb m_tb_start;
    _timeb m_tb_end;
#endif
#ifdef DBG_LOG_WX_USE
	wxTextCtrlTS *m_text_ctrl = nullptr;
#endif
};

/*! \brief Mark the beginning of a log transaction
 * A log transaction is atomic, meaning that it will be logged in one block, even if multiple threads use the same
 * logger.
 */
LOGMOD_API logger &begin(logger &mylog);
/*! \brief Mark the end of a log transaction
 * A log transaction is atomic, meaning that it will be logged in one block, even if multiple threads use the same
 * logger.
 */
LOGMOD_API logger &end(logger &mylog);

LOGMOD_API logger &time(logger &mylog);

//Following needed by GCC
template<class T>
logmod::logger& operator<< (logmod::logger& os, const T &val)
{
    if (logmod::logger::m_old_way)
    {
        if (os.m_os!=NULL)
            (*os.m_os) << val;
    }
    return os;
}

template<class T>
logmod::logger& operator<< (logmod::logger& os, T &val)
{
    std::ostringstream os_str;
    std::string str;

    if (logmod::logger::m_old_way)
    {
        if (os.m_os!=NULL)
            (*os.m_os) << val;
    }
    else
    {
        if (os.m_os!=NULL)
        {
            os_str << val;
            str=os_str.str();
            os.AddDepth(str,os.GetHeaderEnable());
            (*os.m_os) << os_str.str();
        }
    }
    return os;
}

}




#endif
