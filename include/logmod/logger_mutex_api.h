/*!
 * \file logmod/logger_mutex_api.h
 * \brief 
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 *  \endcond
 */

#ifndef __LOGGER_LOGGER_MUTEX_API_H__
#define __LOGGER_LOGGER_MUTEX_API_H__

#include "logmod/logmod_defs.h"

namespace logmod
{

class LOGMOD_API logger_mutex_api
{
public:
	virtual ~logger_mutex_api();
	virtual int Lock() = 0;
	virtual int Unlock() = 0;
};

}

#endif



