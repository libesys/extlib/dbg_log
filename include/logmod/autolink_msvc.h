/*!
 * \file logmod/autolink_msvc.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * __legal_e__
 * \endcond
 *
 */

/*
Before including this header you must define one or more of define the following macros:

LOGMOD_LIB_NAME:           Required: A string containing the basename of the library,
                           for example boost_regex.
LOGMOD_LIB_TOOLSET:        Optional: the base name of the toolset.
LOGMOD_DYN_LINK:           Optional: when set link to dll rather than static library.
LOGMOD_LIB_DIAGNOSTIC:     Optional: when set the header will print out the name
                           of the library selected (useful for debugging).
*/

/*
Libraries for Microsoft compilers are automatically
selected here, the name of the lib is selected according to the following
formula :

LOGMOD_LIB_PREFIX
+ LOGMOD_LIB_NAME
+ "_"
+ LOGMOD_LIB_TOOLSET
+ LOGMOD_LIB_RT_OPT
+ LOGMOD_LIB_ARCH
"-"
+ LOGMOD_LIB_VERSION

These are defined as :
    LOGMOD_LIB_PREFIX  : "lib" for static libraries otherwise "".
    LOGMOD_LIB_NAME    : The base name of the lib(for example boost_regex).
    LOGMOD_LIB_TOOLSET : The compiler toolset name(vc6, vc7, bcb5 etc).
    LOGMOD_LIB_RT_OPT  : A suffix that indicates the runtime library used,
                         contains one or more of the following letters after a hyphen :
                         g      debug / diagnostic runtime(release if not present).
                         d      debug build(release if not present).

    LOGMOD_LIB_ARCH    : The architecture and address model
                                 (-x32 or -x64 for x86 / 32 and x86 / 64 respectively)

    LOGMOD_LIB_VERSION : The Boost version, in the form x_y, for Boost version x.y.


*/

#define LOGMOD_STRINGIZE(X) LOGMOD_DO_STRINGIZE(X)
#define LOGMOD_DO_STRINGIZE(X) #X

#ifdef _MSC_VER

#ifndef LOGMOD_LIB_TOOLSET
#if _MSC_VER < 1200
// Note: no compilers before 1200 are supported
#elif _MSC_VER < 1300

#elif _MSC_VER < 1310
// vc7:
#define LOGMOD_LIB_TOOLSET "vc7"

#elif _MSC_VER < 1400
// vc71:
#define LOGMOD_LIB_TOOLSET "vc71"

#elif _MSC_VER < 1500
// vc80:
#define LOGMOD_LIB_TOOLSET "vc80"

#elif _MSC_VER < 1600
// vc90:
#define LOGMOD_LIB_TOOLSET "vc90"

#elif _MSC_VER < 1700
// vc10:
#define LOGMOD_LIB_TOOLSET "vc100"

#elif _MSC_VER < 1800
// vc11:
#define LOGMOD_LIB_TOOLSET "vc110"

#elif _MSC_VER < 1900
// vc12:
#define LOGMOD_LIB_TOOLSET "vc120"

#elif _MSC_VER < 1910
// vc14:
#define LOGMOD_LIB_TOOLSET "vc140"

#elif _MSC_VER < 1920
// vc14.1:
#define LOGMOD_LIB_TOOLSET "vc141"

#else
// vc14.2:
#define LOGMOD_LIB_TOOLSET "vc142"
#endif

#endif

#if defined(_M_IX86)
#define LOGMOD_LIB_ARCH "-x32"
#elif defined(_M_X64)
#define LOGMOD_LIB_ARCH "-x64"
#elif defined(_M_ARM)
#define LOGMOD_LIB_ARCH "-a32"
#elif defined(_M_ARM64)
#define LOGMOD_LIB_ARCH "-a64"
#endif

#if defined(_DEBUG)
#define LOGMOD_LIB_RT_OPT "-gd"
#else
#define LOGMOD_LIB_RT_OPT
#endif

#if defined(_DLL) && (defined(LOGMOD_DYN_LINK) || !defined(LOGMOD_STATIC_LINK))
#define LOGMOD_LIB_PREFIX
#elif defined(LOGMOD_DYN_LINK)
#error "Mixing a dll boost library with a static runtime is a really bad idea..."
#else
#define LOGMOD_LIB_PREFIX "lib"
#endif

#if defined(LOGMOD_LIB_NAME) && defined(LOGMOD_LIB_PREFIX) && defined(LOGMOD_LIB_TOOLSET) && defined(LOGMOD_LIB_RT_OPT) \
    && defined(LOGMOD_LIB_ARCH) && defined(LOGMOD_LIB_VERSION)
#else
#ifndef LOGMOD_LIB_NAME
#pragma message("LOGMOD_LIB_NAME not defined")
#endif

#ifndef LOGMOD_LIB_PREFIX
#pragma message("LOGMOD_LIB_PREFIX not defined")
#endif

#ifndef LOGMOD_LIB_TOOLSET
#pragma message("LOGMOD_LIB_TOOLSET not defined")
#endif

#ifndef LOGMOD_LIB_RT_OPT
#pragma message("LOGMOD_LIB_RT_OPT not defined")
#endif

#ifndef LOGMOD_LIB_ARCH
#pragma message("LOGMOD_LIB_ARCH not defined")
#endif

#ifndef LOGMOD_LIB_VERSION
#pragma message("LOGMOD_LIB_VERSION not defined")
#endif

#error "some required macros where not defined (internal logic error)."
#endif

#pragma comment(lib, LOGMOD_LIB_PREFIX LOGMOD_STRINGIZE(LOGMOD_LIB_NAME) "-" LOGMOD_LIB_TOOLSET LOGMOD_LIB_RT_OPT LOGMOD_LIB_ARCH \
                     "-" LOGMOD_LIB_VERSION ".lib")
#ifdef LOGMOD_LIB_DIAGNOSTIC
#pragma message("Linking to lib file: " LOGMOD_LIB_PREFIX LOGMOD_STRINGIZE( \
    LOGMOD_LIB_NAME) "-" LOGMOD_LIB_TOOLSET LOGMOD_LIB_RT_OPT LOGMOD_LIB_ARCH "-" LOGMOD_LIB_VERSION ".lib")
#endif

#endif
