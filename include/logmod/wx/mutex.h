/*!
 * \file logmod/wx/mutex.h
 * \brief Mutex implemented with wxWidgets
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2016 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#ifndef __LOGGER_WX_MUTEX_H__
#define __LOGGER_WX_MUTEX_H__

#include "logmod/logmod_defs.h"
#include "logmod/logger_mutex_api.h"

#ifdef DBG_LOG_WX_USE
#include <wx/thread.h>
#include <wx/textctrl.h>

namespace logmod
{

class LOGMOD_API mutex: public logger_mutex_api
{
public:
    mutex();
    virtual ~mutex();

    virtual int Lock();
    virtual int Unlock();

    wxMutex &int_mutex();
protected:
    wxMutex m_mutex;
};


class mutex_locker : public wxMutexLocker
{
public:
    mutex_locker(mutex &mux) :wxMutexLocker(mux.int_mutex())
    {
    }
    virtual ~mutex_locker()
    {
    }
};

}

#endif

#endif

