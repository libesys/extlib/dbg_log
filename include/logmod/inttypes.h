/*!
 * \file logmod/inttypes.h
 * \brief Defines some utilities to handle events
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 */

#ifndef __LOGMOD_INTTYPES_H__
#define __LOGMOD_INTTYPES_H__

#if defined __GNUC__ || defined __GNUG__
#ifdef HAVE_STDINT_H
#include <stdint.h>
#elif defined(HAVE_INTTYPES_H)
#include <inttypes.h>
#else
#include <stdint.h>
#endif
#include <limits.h>
#define UINT32_T_MAX ULONG_MAX
#endif

#ifdef _MSC_VER
#if _MSC_VER < 1600
#include <limits.h>

typedef __int8 int8_t;
typedef __int16 int16_t;
typedef __int32 int32_t;
typedef __int64 int64_t;

typedef unsigned __int8 uint8_t;
typedef unsigned __int16 uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;

#define UINT32_T_MAX ULONG_MAX
#else
#include <stdint.h>
#endif

#endif //_MSC_VER

namespace logmod {

typedef ::int8_t int8_t;
typedef ::int16_t int16_t;
typedef ::int32_t int32_t;
typedef ::int64_t int64_t;

typedef ::uint8_t uint8_t;
typedef ::uint16_t uint16_t;
typedef ::uint32_t uint32_t;
typedef ::uint64_t uint64_t;

}

#endif
