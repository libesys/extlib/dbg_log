/*!
 * \file logmod/event_types.h
 * \brief Define some event_types
 *
 * \cond
 * __legal_b__
 * 
 * Copyright (c) 2014 Michel Gillet  
 * Distributed under the wxWindows Library Licence, Version 3.1. 
 * (See accompanying file LICENSE_3_1.txt or 
 * copy at http://www.wxwidgets.org/about/licence)
 * 
 * __legal_e__
 * \endcond
 */

#ifndef __LOGGER_EVENT_TYPES_H__
#define __LOGGER_EVENT_TYPES_H__

#include "logmod/logmod_defs.h"

#include "logmod/event_type.h"

namespace logmod
{

namespace evt_typ
{

DEFINE_LOGGER_EVENT_TYPE(LOGMOD_API,EVT_NONE);
DEFINE_LOGGER_EVENT_TYPE(LOGMOD_API,EVT_DBG_CLASS);
DEFINE_LOGGER_EVENT_TYPE(LOGMOD_API,EVT_BLOB);
DEFINE_LOGGER_EVENT_TYPE(LOGMOD_API,EVT_LOGGER_EVENT);
DEFINE_LOGGER_EVENT_TYPE(LOGMOD_API,EVT_OPTION_EVENT);

}

}

#endif

