/*!
 * \file logmod/mutex.h
 * \brief
 *
 * \cond
 * __legal_b__
 *
 * Copyright (c) 2014 Michel Gillet
 * Distributed under the wxWindows Library Licence, Version 3.1.
 * (See accompanying file LICENSE_3_1.txt or
 * copy at http://www.wxwidgets.org/about/licence)
 *
 * __legal_e__
 * \endcond
 */

#ifndef __LOGMOD_MUTEX_H__
#define __LOGMOD_MUTEX_H__

#ifdef DBG_LOG_WX_USE
#include "logmod/wx/mutex.h"
#elif defined(DBG_LOG_BOOST_USE)
#include "logmod/boost/mutex.h"
#else
#endif

#endif

