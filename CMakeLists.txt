
if( DEFINED DBG_LOG_EXT_PROJ)

project (dbg_log C CXX )
message(STATUS "dbg_log CMakeLists.txt")
    
get_filename_component (DBG_LOG_ROOT "${CMAKE_BINARY_DIR}/external/dbg_log" REALPATH)
set (DBG_LOG_ROOT ${DBG_LOG_ROOT} CACHE STRING "" FORCE)
message(STATUS "Installing dbg_log to " ${DBG_LOG_ROOT})

# run bootsrap if configure file not found
if( ( NOT (EXISTS ${ENVIRONMENT_ROOT}/external/dbg_log/configure ) ) OR (DEFINED BOOTSTRAP) )
message(STATUS "Running bootstrap command")

set(BOOTSTRAP_CMD "./config/bootstrap")

execute_process(COMMAND "${BOOTSTRAP_CMD}"
                WORKING_DIRECTORY  ${ENVIRONMENT_ROOT}/external/dbg_log
                RESULT_VARIABLE BS_RESULT
                OUTPUT_VARIABLE BS_OUTPUT
                ERROR_VARIABLE BS_ERROR)

if(NOT BS_RESULT EQUAL 0)
    message(FATAL_ERROR "Failed running ${BOOTSTRAP_CMD} in ${ENVIRONMENT_ROOT}/external/dbg_log:\n${BS_OUTPUT}\n${BS_ERROR}\n")
else()
    #printout bootstrap output
    message(STATUS "${BS_OUTPUT}\n${BS_ERROR}\n")
endif()
else()
message(STATUS "${ENVIRONMENT_ROOT}/external/dbg_log/configure exists, not running bootstrap command")
endif()

include(ExternalProject)

if(DEFINED DBG_LOG_WX_CONFIG_DIR)
set (WX_CONFIG_BIN --with-wx-config=${DBG_LOG_WX_CONFIG_DIR}/wx-config)
message(STATUS "DBG_LOG using a not installed config scrpt " ${WX_CONFIG_BIN})
endif()

ExternalProject_Add(
  dbg_log  
  #DEPENDS wx
  SOURCE_DIR ${ENVIRONMENT_ROOT}/external/dbg_log
  CONFIGURE_COMMAND ${ENVIRONMENT_ROOT}/external/dbg_log/configure --prefix=${DBG_LOG_CMAKE_PREFIX} ${WX_CONFIG_BIN}
  PREFIX ${DBG_LOG_CMAKE_PREFIX}
  BUILD_COMMAND $(MAKE)
  BUILD_IN_SOURCE 0
  INSTALL_COMMAND $(MAKE) install
)

message(STATUS "dbg_log_PREFIX = " ${dbg_log_PREFIX})

SET( LOGMOD_INC ${DBG_LOG_CMAKE_PREFIX}/include  CACHE STRING "" FORCE)
SET( DBG_LOG_INC ${DBG_LOG_CMAKE_PREFIX}/include  CACHE STRING "" FORCE)

SET( LOGMOD_LIBRARY ${DBG_LOG_CMAKE_PREFIX}/lib/liblogmod.so  CACHE STRING "" FORCE)
SET( DBG_LOG_LIBRARY ${DBG_LOG_CMAKE_PREFIX}/lib/libdbg_log.so  CACHE STRING "" FORCE)

else()

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_subdirectory(src)
endif()

get_filename_component (DBG_LOG "${CMAKE_BINARY_DIR}" REALPATH)
