#!/bin/sh

autoreconf -I m4 --install -v
automake --add-missing --copy >/dev/null 2>&1
