from esysbuild.lib import *
from esysbuild.buildfile import *

class MyDbgLogBuildFile(BuildFile):
    def __init__(self):
        super().__init__()
        self.SetBootStrapScript("config/bootstrap")
        
    def AddLibs(self, lib_mngr):   
    
        dbg_log=Lib("dbg_log")
        dbg_log.AddGroupName("vhw")
        dbg_log.SetLinkCfg(Lib.LINK_LIBTOOL, "src/dbg_log/libdbg_log.la")
        dbg_log.SetLinkCfg(Lib.LINK_INSTALLED, "-ldbg_log")
        lib_mngr.Add(dbg_log)
                
        logmod=Lib("logmod")
        logmod.AddGroupName("vhw")
        logmod.SetLinkCfg(Lib.LINK_LIBTOOL, "src/logmod/liblogmod.la")
        logmod.SetLinkCfg(Lib.LINK_INSTALLED, "-llogmod")
        lib_mngr.Add(logmod)
        return 0
    
def GetBuildFile():
    return MyDbgLogBuildFile()



    